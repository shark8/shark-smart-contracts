## SHARK project smart contracts

`SHARKToken` - BEP/ERC-20 compatible token
`SharkKeeper` - BEP/ERC-721 compatible NFT for store IPFS hashes

### Tests
We use Brownie framework for developing and unit test. For run tests
first please [install it](https://eth-brownie.readthedocs.io/en/stable/install.html)

```bash
brownie pm install OpenZeppelin/openzeppelin-contracts@4.1.0
brownie test
```
Don't forget [ganache-cli](https://www.npmjs.com/package/ganache-cli)

### Deployments

#### Rinkeby TestNet 20210617
**SHARKToken**
https://rinkeby.etherscan.io/address/0xafA82DB88719C8CD4A3c368170ca003edB1D6269#code  

**SharkKeeper**
https://rinkeby.etherscan.io/address/0x77E06E7175F989140AC42E95E9F986E5C990cb7d#code
