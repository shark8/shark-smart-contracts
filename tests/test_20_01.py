import pytest
import logging
from brownie import Wei, reverts
LOGGER = logging.getLogger(__name__)

# def test_first(pool_repo):
# 	assert pool_repo.getPoolsCount() == '1'
IPFS_HASHES = ['QmafZq1ZYLGvQJKAVt8PM3Y78to5wQrsazyasqdBCB9XU9','QmPYo5x5WV9zh8XybCiwQQGFPzpnZepBwHFkHbf2kLBKe8']
TOTAL_SUPPLY = 1000000e18
def test_20mock(accounts, erc20):
    assert erc20.totalSupply()== TOTAL_SUPPLY
    assert erc20.balanceOf(accounts[0]) == TOTAL_SUPPLY

def test_20_transfer_fail(accounts, erc20):
    with reverts('ERC20: transfer amount exceeds balance'):
        erc20.transfer(accounts[1], TOTAL_SUPPLY*2, {'from':accounts[0]}) 

    # logging.info(' erc20.balanceOf(0) {}'.format(
    #     erc20.balanceOf(accounts[0])
    # ))
    # logging.info(' erc20.balanceOf(1) {}'.format(
    #     erc20.balanceOf(accounts[1])
    # ))

    # logging.info(' tx {}'.format(
    #     tx.events
    # ))
