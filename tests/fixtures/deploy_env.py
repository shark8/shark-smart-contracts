import pytest

@pytest.fixture(scope="module")
def erc20(accounts, SHARKToken):
    e = accounts[0].deploy(SHARKToken,"SHARKToken", "SHRK", 1000000e18)
    yield e

@pytest.fixture(scope="module")
def erc721(accounts, SharkKeeper):
    """
    Simple NFT with URI
    """
    t = accounts[0].deploy(SharkKeeper, "Shark project IPFS hash storage", "shrkNFT")
    yield t    
