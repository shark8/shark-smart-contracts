import pytest
import logging
from brownie import Wei, reverts
LOGGER = logging.getLogger(__name__)

# def test_first(pool_repo):
# 	assert pool_repo.getPoolsCount() == '1'
IPFS_HASHES = ['QmafZq1ZYLGvQJKAVt8PM3Y78to5wQrsazyasqdBCB9XU9','QmPYo5x5WV9zh8XybCiwQQGFPzpnZepBwHFkHbf2kLBKe8']

def test_721mock(accounts, erc721):
    assert erc721.balanceOf(accounts[0]) == 0
    erc721.mintWithURI(IPFS_HASHES[0], {'from':accounts[0]})
    assert erc721.balanceOf(erc721.address) == 1
    logging.info(' erc721.tokenURI {}'.format(
        erc721.tokenURI(erc721.lastSavedHashId())
    ))

    logging.info(' erc721.ownerOf {}'.format(
        erc721.ownerOf(erc721.lastSavedHashId())
    ))
    erc721.returnTo(accounts[1], erc721.lastSavedHashId(), {'from':accounts[0]})
    assert erc721.balanceOf(accounts[1]) == 1

def test_721_transfer(accounts, erc721):
    erc721.transferFrom(accounts[1],accounts[2],erc721.lastSavedHashId(), {'from':accounts[1]}) 
    assert erc721.balanceOf(accounts[2]) == 1

def test_mint_fail(accounts, erc721):
    with reverts('Only for trusted minters'):
        erc721.mintWithURI(IPFS_HASHES[0], {'from':accounts[1]})

def test_mint_fail(accounts, erc721):
    with reverts('Only for trusted minters'):
        erc721.returnTo(accounts[1], erc721.lastSavedHashId(), {'from':accounts[1]})

def test_transferFrom_fail(accounts, erc721):
    with reverts('ERC721: transfer of token that is not own'):
        erc721.transferFrom(accounts[2], accounts[1], erc721.lastSavedHashId(), {'from':accounts[0]})
